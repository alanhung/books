use clap::{Parser, Subcommand};
use open as xdg_open;
use std::{
    fs::{self, File},
    io::{Read, Write},
};

#[derive(Parser, Debug)]
#[command(author, version, about, propagate_version = true)]
struct Args {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    #[command(alias = "i")]
    Insert { book_path: String },
    #[command(alias = "o")]
    Open { book_path: String },
    #[command(alias = "g")]
    Get { book_path: String },
}

fn main() -> Result<(), anyhow::Error> {
    let Some(mut path) = dirs::home_dir() else {
        return Err(anyhow::anyhow!("Home directory not found"))
    };
    path.push("Books");

    let arguments = Args::parse();

    match arguments.command {
        Commands::Get { book_path } => {
            path.push(book_path);
            println!("{}", path.to_str().unwrap());
        }
        Commands::Open { book_path } => {
            path.push(book_path);
            xdg_open::that(path)?;
        }
        Commands::Insert { book_path } => {
            path.push(book_path);

            fs::create_dir_all(path.parent().unwrap())?;
            let mut file = File::options().write(true).create_new(true).open(path)?;

            let mut stdin = std::io::stdin();
            let mut contents = Vec::new();
            stdin.read_to_end(&mut contents)?;

            file.write_all(&contents)?;
        }
    }
    Ok(())
}
