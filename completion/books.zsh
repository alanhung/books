#compdef books

_books_complete_commands() {
    local -a subcommands
    subcommands=(
        "help:get help"
        "open:open file"
        "get:get file path"
        "insert:insert new file at path"
    )
    _describe -t commands 'books' subcommands
}

__books_complete_directories() {
    local prefix=$HOME/Books/
    local directories=$prefix$cur
    _values 'directories' ${$(find -L "$prefix" -type d 2>/dev/null | sed "s#${prefix}##" ):-""}
}

__books_complete_paths () {
    local prefix=$HOME/Books/
    local directories=$prefix$cur
    _values 'paths' ${$(find -L "$prefix" -type f 2>/dev/null | sed "s#${prefix}##" ):-""}
}

_books () {
    local cur="${words[CURRENT]}"
    if (( CURRENT <= 2 )); then
        _books_complete_commands
    else
        case "${words[2]}" in
            open|o|get|g)
                __books_complete_paths
                ;;
            insert|i)
                __books_complete_directories
                ;;
        esac
    fi
    _arguments "--version[Output version information]" "-V[Output version information]" "--help[Output help message]" "-h[Output help message]"
}

_books
