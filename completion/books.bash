_books_complete_commands()
{
    local commands="i insert o open g get help"
    COMPREPLY+=($(compgen -W "${commands}" -- "${cur}"))
}

_books_complete_directories ()
{
    local prefix="$HOME/Books/"
    local directories=($(compgen -d "$prefix$cur"))
    local i=0 directory
    for directory in ${directories[@]}; do
        directory="$directory/"
        COMPREPLY+=("${directory#$prefix}")
        let i+=1
    done

    if [[ $i -eq 1 && -d $directory ]]; then
        compopt -o nospace
    fi
}

_books_complete_paths ()
{
    local prefix="$HOME/Books/"
    local files=($(compgen -f "$prefix$cur"))
    local i=0 file
    for file in "${files[@]}"; do
        [[ -d $file ]] && file="$file/"
        COMPREPLY+=("${file#$prefix}")
        let i+=1
    done

    if [[ $i -eq 1 && -d $file ]]; then
        compopt -o nospace
    fi

}

_books()
{
    COMPREPLY=()
    local cur="${COMP_WORDS[COMP_CWORD]}"
    if [[ $COMP_CWORD -le 1 ]] ; then
        _books_complete_commands
    else
        case "${COMP_WORDS[1]}" in
            open|o|get|g)
                _books_complete_paths
                ;;
            insert|i)
                _books_complete_directories
                ;;
        esac
    fi
    COMPREPLY+=($(compgen -W "--help -h --version -V" -- ${cur}) )
}

complete -o filenames -F _books books
